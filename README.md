# Forecast App

A simple web application, which shows the weather forecast for the upcoming five days in any given location. Supports input using geolocation. Results include the weather, the temperature during the day and the night, the time of sunrise and sunset, as well as a chart of the temperature over the next 5 days, in intervals of 3 hours.

The latest version is available at [forecast.cingel.sk](https://forecast.cingel.sk/).

## Project setup

To work on this project, you need to have Node.js and npm installed. Once these prerequisites are satisfied, traverse to the root directory of the project using terminal, and enter the following command to set up the environment:

    npm install

### Compiles and hot-reloads for development

    npm run dev

### Compiles and minifies for production

    npm run build

The deployment-ready files can be found in the */dist* directory.

## Browser support

This application has been tested on the latest versions of Google Chrome, Mozilla Firefox, Microsoft Edge and Safari. Devices with smaller screen sizes are also supported.

## Description

This application is written in TypeScript and uses the Vite build tools. No third party libraries are contained within the application itself. Styles are pre-processed using Sass.

### search.ts

The core component of this application is the search bar, with its keyboard-navigable dropdown list. All of this functionality, as well as geolocation, is contained within the *search.ts* file. Text input into the field calls the *updateDropdown* function, which filters the JSON data from *city.list.json.gz* and shows 10 results to the user in a dropdown. After the user chooses a location, *updateForecast* from *forecast.ts* and *updateGraph* from *graph.ts* get called with its coordinates as arguments.

### forecast.ts

The *updateForecast* function fetches data from [OpenWeather One Call API](https://openweathermap.org/api/one-call-api), which get processed by the *parseData* function. After an element is created for each day in the *createElement* function, these elements are inserted into DOM and displayed to the user.

### graph.ts

This file contains all the functionality of the temperature chart. The *updateGraph* function fetches data from [OpenWeather 5 day weather forecast](https://openweathermap.org/forecast5). The data is then parsed using the *parseData* function. *drawGrid* draws the background of the chart, and *drawLine* adds the temperature line on top of it. The chart is drawn using the HTML5 Canvas API.

### locations.ts

This file contains the definition of type *Location*, which gets used in search.ts, as well as the function for retrieving the data from gzipped *city.list.json.gz* file.
