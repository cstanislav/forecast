const forecastElement = document.querySelector<HTMLElement>('.forecast')!;
const loadingElement = document.querySelector<HTMLElement>('.forecast__loading')!;
const listElement = document.querySelector<HTMLElement>('.forecast__list')!;

type Day = {
  dt: number,
  sunrise: number,
  sunset: number,
  temp: {
    day: number,
    night: number
  },
  weather: {
    icon: string,
    main: string
  }[]
};

type Response = {
  daily: Day[],
  timezone: string
};

function createElement(day: Day, timezone: string): Element {
  const date = new Date(day.dt * 1000);
  const dateString = date.toLocaleDateString([], { timeZone: timezone });

  const sunrise = new Date(day.sunrise * 1000);
  const sunriseTimeString = sunrise.toLocaleTimeString([], { timeStyle: 'short', timeZone: timezone });

  const sunset = new Date(day.sunset * 1000);
  const sunsetTimeString = sunset.toLocaleTimeString([], { timeStyle: 'short', timeZone: timezone });

  const template = document.createElement('template');
  template.innerHTML = `<li class="forecast__item">
      <div class="forecast__date">${dateString}</div>
      <div class="forecast__weather">
        <div class="forecast__icon">
          <img class="forecast__img"
            src="https://openweathermap.org/img/wn/${day.weather[0].icon}@2x.png"
            alt=""
          >
        </div>
        ${day.weather[0].main}
      </div>
      <div class="forecast__groups">
        <div class="forecast__group">
          <div class="forecast__label">Day</div>
          <div class="forecast__value">${Math.round(day.temp.day)} °C</div>
        </div>
        <div class="forecast__group">
          <div class="forecast__label">Night</div>
          <div class="forecast__value">${Math.round(day.temp.night)} °C</div>
        </div>
        <div class="forecast__group">
          <div class="forecast__label">Sunrise</div>
          <div class="forecast__value">${sunriseTimeString}</div>
        </div>
        <div class="forecast__group">
          <div class="forecast__label">Sunset</div>
          <div class="forecast__value">${sunsetTimeString}</div>
        </div>
      </div>
    </li>`;

  return template.content.firstElementChild!;
}

function clearForecast(): void {
  listElement.innerHTML = '';
}

function parseData(data: Response): void {
  // Truncate to 5 days
  const days = data.daily.slice(0, 5);

  // Create a li element for each day
  const elements = days.map((day) => createElement(day, data.timezone));

  // Append created elements to list element
  elements.forEach((element) => {
    listElement.appendChild(element);
  });
}

function updateForecast(lat: number, lon: number): void {
  clearForecast();

  loadingElement.classList.add('forecast__loading_active');
  forecastElement.classList.add('forecast_active');

  const url = `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&appid=9a6ec5f94f4a11fd176f96b945be1e88&units=metric`;

  fetch(url).then((response) => response.json()).then((data) => {
    loadingElement.classList.remove('forecast__loading_active');

    clearForecast(); // Fixes the situation in which a slow response arrives after clearForecast

    parseData(data);
  });
}

export { updateForecast };
