const graphElement = document.querySelector<HTMLElement>('.graph')!;
const loadingElement = document.querySelector<HTMLElement>('.graph__loading')!;
const graphCanvas = document.querySelector<HTMLCanvasElement>('.graph__canvas')!;
const ctx = graphCanvas.getContext('2d')!;

const graphLeft = 50;
const graphRight = 950;
const graphTop = 20;
const graphBottom = 230;

type Point = {
  dt: number,
  main: {
    temp: number
  }
};

type GraphResponse = {
  city: {
    timezone: number
  }
  list: Point[]
};

function drawGrid(minTemp: number, maxTemp: number, step: number, data: GraphResponse): void {
  ctx.lineWidth = 1;
  ctx.strokeStyle = '#aeaeae';

  // Draw axes
  ctx.beginPath();
  ctx.moveTo(graphLeft, graphTop);
  ctx.lineTo(graphLeft, graphBottom);
  ctx.lineTo(graphRight, graphBottom);
  ctx.stroke();

  // Label min/max temperatures
  ctx.font = '12px Arial';
  ctx.fillText(`${Math.round(minTemp)} °C`, 10, graphBottom + 5);
  ctx.fillText(`${Math.round(maxTemp)} °C`, 10, graphTop + 5);

  // Draw columns
  ctx.lineWidth = 1;
  ctx.strokeStyle = '#dedede';
  ctx.setLineDash([4, 4]);

  data.list.forEach((point, i) => {
    const x = graphLeft + step * i;

    if (i !== 0 && i % 2 === 0) {
      ctx.beginPath();
      ctx.moveTo(x, graphTop);
      ctx.lineTo(x, graphBottom);
      ctx.stroke();
    }

    if (i % 4 === 0) {
      const datetime = new Date((point.dt + data.city.timezone) * 1000);
      const date = datetime.toLocaleDateString([], { dateStyle: 'short' });
      const time = datetime.toLocaleTimeString([], { timeStyle: 'short' });

      ctx.font = '12px Arial';

      ctx.fillText(date, x, graphBottom + 20);
      ctx.fillText(time, x, graphBottom + 35);
    }
  });

  ctx.setLineDash([]); // Reset line dash
}

function drawLine(minTemp: number, maxTemp: number, step: number, data: GraphResponse) {
  function stepTempToCoords(i: number, temp: number): [number, number] {
    // Helper function for easier transformation of data to coordinates
    const x = graphLeft + step * i;
    const y = graphTop + ((graphBottom - graphTop) / (maxTemp - minTemp)) * (maxTemp - temp);

    return [x, y];
  }

  ctx.lineWidth = 2;
  ctx.strokeStyle = '#2049ff';

  ctx.beginPath();

  data.list.forEach((point, i) => {
    if (i === 0) {
      ctx.moveTo(...stepTempToCoords(0, data.list[0].main.temp));
    } else {
      ctx.lineTo(...stepTempToCoords(i, point.main.temp));
    }
  });

  ctx.stroke();
}

function parseData(data: GraphResponse): void {
  // Get lowest temperature
  const minTemp: number = Math.min(...data.list.map(
    (point) => point.main.temp,
  ));

  // Get highest temperature
  const maxTemp: number = Math.max(...data.list.map(
    (point) => point.main.temp,
  ));

  // Calculate width of a single "segment" in graph (3 hours)
  const step = (graphRight - graphLeft) / data.list.length;

  drawGrid(minTemp, maxTemp, step, data);
  drawLine(minTemp, maxTemp, step, data);
}

function clearGraph(): void {
  ctx.clearRect(0, 0, graphCanvas.width, graphCanvas.height);
}

function updateGraph(lat: number, lon: number): void {
  clearGraph();

  loadingElement.classList.add('graph__loading_active');
  graphElement.classList.add('graph_active');

  const url = `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}&appid=9a6ec5f94f4a11fd176f96b945be1e88&units=metric`;

  fetch(url).then((response) => response.json()).then((data) => {
    loadingElement.classList.remove('graph__loading_active');
    parseData(data);
  });
}

export { updateGraph };
