import { fetchLocations, Location } from './locations';
import { updateForecast } from './forecast';
import { updateGraph } from './graph';

const inputElement = document.querySelector<HTMLInputElement>('.search__input')!;
const geolocateButton = document.querySelector<HTMLInputElement>('.search__geolocate')!;
const dropdownElement = document.querySelector<HTMLElement>('.search__dropdown')!;
const listElement = document.querySelector<HTMLElement>('.search__list')!;

let dropdown: Location[] = [];
let currentItem = 0;
let locations: Location[] = [];

function selectCity(city: Location): void {
  // If attribute "country" is not empty, join it with "name" using a comma
  inputElement.placeholder = city.country
    ? `${city.name}, ${city.country}`
    : city.name;

  inputElement.value = '';

  updateForecast(city.coord.lat, city.coord.lon);
  updateGraph(city.coord.lat, city.coord.lon);
}

function geolocate(): void {
  inputElement.placeholder = 'Geolocating...';

  navigator.geolocation.getCurrentPosition(
    (position) => {
      // Success
      const location = {
        coord: {
          lat: position.coords.latitude,
          lon: position.coords.longitude,
        },
        name: `${position.coords.latitude}, ${position.coords.longitude}`,
        state: '',
        country: '',
        id: NaN,
      };
      selectCity(location);
    },
    () => {
      // Failure
      inputElement.placeholder = 'Your location couldn\'t be found.';
    },
  );
}

function clearDropdown(): void {
  // Remove dropdown items and close the dropdown
  listElement.innerHTML = '';
  dropdownElement.classList.remove('search__dropdown_open');
}

function createElement(city: Location): Element {
  const template = document.createElement('template');
  template.innerHTML = `<li class="search__item">${city.name}, ${city.country}</li>`;

  const element = template.content.firstElementChild!;

  // Add click event listener
  element.addEventListener('mousedown', () => {
    selectCity(city);
  }, true);

  return element;
}

function updateItems() {
  if (currentItem > dropdown.length - 1) {
    currentItem = 0;
  }

  const items = document.getElementsByClassName('search__item');

  if (dropdown.length === 0 || items.length === 0) {
    return;
  }

  Array.from(items).forEach((item) => {
    item.classList.remove('search__item_current');
  });

  items[currentItem].classList.add('search__item_current');
}

function updateDropdown(): void {
  clearDropdown();

  const queryString: string = inputElement.value;

  if (queryString.trim() === '' || locations.length === 0) {
    return;
  }

  const filtered = locations.filter(
    (city) => city.name.toLowerCase().includes(queryString.toLowerCase()),
  );

  // Limit search results to 10
  dropdown = filtered.slice(0, 10);

  if (filtered.length === 0) {
    // No results
    return;
  }

  // Create li elements
  const elements = dropdown.map((city) => createElement(city));

  // Append li elements to list
  elements.forEach((element) => {
    listElement.appendChild(element);
  });

  updateItems();

  dropdownElement.classList.add('search__dropdown_open');
}

function keyPress(e: KeyboardEvent) {
  if (['ArrowUp', 'ArrowDown', 'Enter'].indexOf(e.code) > -1) {
    // Prevent scrolling
    e.preventDefault();
  }

  if (e.code === 'ArrowUp') {
    if (currentItem > 0) {
      currentItem--;
    } else {
      currentItem = Math.max(dropdown.length - 1, 0);
    }
  } else if (e.code === 'ArrowDown') {
    if (currentItem < dropdown.length - 1) {
      currentItem++;
    } else {
      currentItem = 0;
    }
  } else if (e.code === 'Enter') {
    if (dropdown.length !== 0) {
      selectCity(dropdown[currentItem]);
      currentItem = 0;
    }
  }

  updateItems();
}

fetchLocations((data) => {
  locations = data;
  updateDropdown();
});

inputElement.addEventListener('keyup', updateDropdown);
inputElement.addEventListener('focusout', clearDropdown);
inputElement.addEventListener('keydown', keyPress);

geolocateButton.addEventListener('click', geolocate);
