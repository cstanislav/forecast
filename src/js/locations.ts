type Location = {
  id: number,
  name: string,
  state: string,
  country: string,
  coord: {
    lon: number,
    lat: number
  }
};

function fetchLocations(callback: (locations: Location[]) => void): void {
  fetch('city.list.json.gz')
    .then((response) => response.text())
    .then((data) => {
      callback(JSON.parse(data));
    });
}

export { Location, fetchLocations };
